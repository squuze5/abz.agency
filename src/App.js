import React from 'react';
import FormPage from './pages/Form';
import MainPage from './pages/Main';

const App = () => {
  return (
    <div className="app">
      <MainPage />
    </div>
  );
}

export default App;
