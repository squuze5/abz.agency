import React from 'react';

/* Components */
import { Navbar } from '../components/Navbar';
import Banner from '../components/Banner/Banner';
import About from '../components/About/About';
import Users from '../components/Users/Users';
import Form from '../components/Form/Form';
import { Footer } from '../components/Footer';

const MainPage = () => {
    return (
        <>
            <Navbar />
            <Banner />
            <About />
            <Users />
            <Form />
            <Footer />
        </>
    )
}

export default MainPage;