import React, {useState} from 'react';
import axios from 'axios';

const baseURL = 'https://frontend-test-assignment-api.abz.agency/api/v1';
const token = 'eyJpdiI6IjJ3QUNUeGhhSlc3NFROZFg3c1ppb0E9PSIsInZhbHVlIjoiajhTUEFJQitqMUdrVGFjbkdxQnZCalwvZTFrWk0yOWhIQmRxSjZxRmU4UHZwa3cwNEVrcjNRWDBoaElUaFNhdTVkSHhBOENjem5GbzFuOVhQT3lIZ2hRPT0iLCJtYWMiOiJlYzg3NTNlYTRmNjNmYjE3OTdjYTlkZGRmOWFkOGZmNmEyYzE4NTZiNTQ5NjYxZTkwZmYxYjQ1YjJlZjI0MDI4In0='

const FormPage = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [position, setPosition] = useState(1);
    const [file, setFile] = useState(null);
    const [error, setError] = useState([]);

    const onChange = (e) => {
        setFile(e.target.files[0]);
    }

    const submitForm = (e) => {
        e.preventDefault();
        const formData = new FormData();

        formData.append("name", name);
        formData.append("email", email);
        formData.append("phone", phone);
        formData.append("position_id", position);
        formData.append("photo", file, file.name);

        axios
            .post(`${baseURL}/users`, formData, {headers: {token: token } })
            .then((res) => {
                alert("File Upload success ");
                console.log(res);
            })
            .catch((err) => {
                alert("File Upload Error ", err);
                setError(err.response.data.fails);
                console.log("Errors: ", err.response.data.fails);
            })
    }

    return (
        <section className="form">
            <div className="container">

                <div className="section__header">
                    <div className="section__header-title">
                        <h2>Register to get a work</h2>
                    </div>
                    <div className="section__header-subtitle">
                        <h5>
                            Attention! After successful registration and alert, 
                            update the list of users in the block from the top
                        </h5>
                    </div>
                </div>

                <form className="form__user" enctype="multipart/form-data">

                    <div className="form__user-group">
                        <label htmlFor="name">Name</label>
                        <input 
                            type="text" 
                            name="name" 
                            id="name"
                            placeholder="Your name"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                        <span>Error message</span>
                    </div>

                    <div className="form__user-group">
                        <label htmlFor="email">Email</label>
                        <input 
                            type="text"
                            name="email"
                            id="email"
                            placeholder="Your email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                        <span>Error message</span>
                    </div>

                    <div className="form__user-group">
                        <label htmlFor="phone">Phone number</label>
                        <input 
                            type="text"
                            name="phone"
                            id="phone"
                            placeholder="+380 XX XXX XX XX"
                            value={phone}
                            onChange={(e) => setPhone(e.target.value)}
                        />
                        <span>Error message</span>
                    </div>

                    <div className="form__user-group radio__group">
                        <label htmlFor="position">Select your position</label>
                        
                        <div className="radio__group-item">
                            <input id="fronend"type="radio" value="fronend" name="position"/>
                            <label htmlFor="fronend">Fronend</label>
                        </div>

                        <div className="radio__group-item">
                            <input id="back"type="radio" value="back" name="position"/>
                            <label htmlFor="back">Backend developer</label>
                        </div>

                        <div className="radio__group-item">
                            <input id="designer"type="radio" value="designer" name="position"/>
                            <label htmlFor="designer">Designer</label>
                        </div>

                        <div className="radio__group-item">
                            <input id="qa"type="radio" value="qa" name="position"/>
                            <label htmlFor="qa">QA</label> 
                        </div>
                    </div>

                    <div className="form__user-group">
                        <label>Photo</label>
                        <label htmlFor="file" className="labelfile">
                            <span>Upload your photo</span>
                            <strong>Browse</strong>
                        </label>
                        
                        <input 
                            type="file" 
                            onChange={(e) => onChange(e)}
                        />
                    </div>

                    <div className="submit__button button-center">
                        <button onClick={submitForm}>Sing up now</button>
                    </div>
                    
                </form>

            </div>
        </section>
    )
}

export default FormPage;