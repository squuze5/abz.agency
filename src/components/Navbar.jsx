import React from 'react';
import Logo from '../assets/img/logo.svg';

export const Navbar = () => {

    return (
        <div className="navigation">
            <div className="container">
                <div className="navigation__content">

                    <div className="navigation__content-logo">
                        <img src={Logo} alt="logo"/>
                    </div>

                    <ul className="navigation__content-items">
                        <li>About me</li>
                        <li>Relationships</li>
                        <li>Requirements</li>
                        <li>Users</li>
                        <li>Sign Up</li>
                    </ul>

                </div>
            </div>
        </div>
    )
}