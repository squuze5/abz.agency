import React, { useRef } from 'react';

const FileUploader = () => {
    const fileInput = useRef(null);

    const handleFileInput = (e) => {
        // handle validations
        const file = e.target.files[0];
        return file;
      };

    return (
        <div className="file-uploader">
            <input type="file" onChange={handleFileInput} />
        </div>
    )
}

export default FileUploader;