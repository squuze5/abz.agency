import React from 'react';

const UserCard = ({user}) => {

    return (
        <div className="user__card">
            <li className="user__card-item">
                <div className="user-card__avatar">
                    <img src={user.photo} alt="User Avatar"/>
                </div>
                <h4 className="user-card__name">
                    {user.name}
                </h4>
                <span className="user-card__position">
                    {user.position}
                </span>
                <span className="user-card-email">
                    {user.email}
                </span>
                <span className="user-card-phone">
                    {user.phone}
                </span>
            </li>
        </div>
    )
}

export default UserCard;