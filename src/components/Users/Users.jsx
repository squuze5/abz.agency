import React from 'react';
import './Users.scss';
import UserCard from './UserCard';

const baseURL = 'https://frontend-test-assignment-api.abz.agency/api/v1';

const Users = () => {
    const [users, setUsers] = React.useState([]);
    const [pageAPI, setPageAPI] = React.useState(0);
    const [page, setPage] = React.useState(1);
    const [loading, setLoading] = React.useState('');

    React.useEffect(() => {
        fetch(`${baseURL}/users?page=${page}&count=6`)
          .then(res => res.json())
          .then(json => {
            setUsers(json.users);
            setPageAPI(json.total_pages);
            setPage(page + 1);
          })
          .catch(err => {
              console.error(err);
          })
    }, []);

    const showMore = async () => {
        console.log(pageAPI + ' ' + page);
        setLoading('loader');
    
        if (page > pageAPI) {
          console.log('the end');
          setLoading('');
        } else {
          setPage(page + 1);
          await fetch(`${baseURL}/users?page=${page}&count=6`)
          .then(res => res.json())
          .then(json => {
            setUsers(prevState => ([...prevState, ...json.users]));
            setLoading('');
          })
          .catch(err => {
            console.error(err);
        })
        }
    }

    return (
        <section className="users">
            <div className="container section">

                <div className="section__header">
                    <div className="section__header-title">
                        <h2>Our cheerful users</h2>
                    </div>
                    
                    <div className="section__header-subtitle">
                        <h5>Attention! Sorting users by registration date</h5>
                    </div>
                </div>

                <div className="users__content">
                    <ul className="users__content-list">
                    {users.length !== 0 ? 
                        users.map(user => (
                            <UserCard key={user.id} user={user} />
                        )) : 
                            <div>Loading...</div>}
                    </ul>

                    <div className="show__more">
                        <button className="show__more-btn" onClick={showMore}>
                            <div className={loading}></div>
                            Show more
                        </button>
                    </div>
                </div>
                
            </div>
        </section>
    )
}

export default Users;